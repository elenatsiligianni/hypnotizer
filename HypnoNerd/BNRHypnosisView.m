//
//  BNRHypnosisView.m
//  Hypnosister
//
//  Created by Elena Tsiligianni on 14/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

#import "BNRHypnosisView.h"

@interface BNRHypnosisView ()

@property (strong, nonatomic) UIColor *circleColor;

@end

@implementation BNRHypnosisView

// SILVER CHALLENGE
-(void)selectCircleColor:(id)sender {
    
    char colorIndex = [sender selectedSegmentIndex];
    switch (colorIndex) {
        case 0:
            self.circleColor = [UIColor redColor];
            break;
        case 1:
            self.circleColor = [UIColor greenColor];
            break;
        case 2:
            self.circleColor = [UIColor blueColor];
            break;
            
        default:
            break;
    }

}

-(void)setCircleColor:(UIColor *)circleColor {
    
    _circleColor = circleColor;
    [self setNeedsDisplay];
}

// When a finger touches the screen
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [super touchesBegan:touches withEvent:event];
    NSLog(@"%@ was touched", self);
    
    // Get 3 random numbers between 0 and 1
    float red = (arc4random() % 100) / 100.0;
    float green = (arc4random() % 100) / 100.0;
    float blue = (arc4random() % 100) / 100.0;
    
    UIColor *randomColor = [UIColor colorWithRed:red
                                           green:green
                                            blue:blue
                                           alpha:1.0];
    
    self.circleColor = randomColor;

}

-(instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        // All BNRHypnosisViews start with a clear background color
        self.backgroundColor = [UIColor clearColor];
        self.circleColor = [UIColor lightGrayColor];
    }
    
    return self;
}


-(void)drawRect:(CGRect)rect {
    
    CGRect bounds = self.bounds;
    
    // Figure out the center of the bounds rectangle
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width / 2.0;
    center.y = bounds.origin.y + bounds.size.height / 2.0;
    
    // The circle will be the largest that will fit in the view
    //float radius = (MIN(bounds.size.width, bounds.size.height) / 2.0);
    
    // The largest circle will circumscribe the view
    float maxRadius = hypot(bounds.size.width, bounds.size.height) / 2.0;
    
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    // Add an arc to the path at center, with radius of radius,
    // from 0 to 2*PI radians (a circle)
//    [path addArcWithCenter:center
//                    radius:radius
//                startAngle:0.0
//                  endAngle:M_PI * 2.0
//                 clockwise:YES];
    for (float currentRadius = maxRadius; currentRadius > 0; currentRadius -= 20) {
        
        [path moveToPoint:CGPointMake(center.x + currentRadius, center.y)];
        
        [path addArcWithCenter:center
                        radius:currentRadius  // Note this is currentRadius
                    startAngle:0.0
                      endAngle:M_PI * 2.0
                     clockwise:YES];
    }
    
    // Configure line width to 10 points
    path.lineWidth = 10;
    
    // Configure the drawing color to light gray
    //[[UIColor lightGrayColor] setStroke];
    [self.circleColor setStroke];
    
    // Draw the line
    [path stroke];
  
    
    
//    // GOLD CHALLENGE
//    
//    
//    //-------- To be used by subsequent code -------
//    /*  Get current image context - CoreGraphics API required here */
//    // Get current drawing context for the view
//    CGContextRef currentContext = UIGraphicsGetCurrentContext();
//    /*  Made a new smaller frame to compensate for the wrong logo image size on new iphone6 simulator */
//    CGRect imageRect = CGRectMake(bounds.origin.x + 60, bounds.origin.y + 60, bounds.size.width / 1.5, bounds.size.height / 1.5);
//    //---------------------------------------------
//    
//    /*  Define a Gradient  - Gradients, like shadows, require CoreGraphics API directly */
//    // Gradients allow you to do shading that moves smoothly through a list of colors.
//    // Build parameters for gradient function
//    CGFloat locations[2] = { 0.0, 1.0 }; // array of floats for location parameter
//    CGFloat components[8] = { 0.0, 1.0, 0.0, 1.0, // Start Color is Red    // array of floats for components parameter
//        1.0, 1.0, 0.0, 1.0 }; // End color is Yellow
//    // Encapsulates color space info - for colorSpace parameter
//    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//    // Build gradient
//    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, components, locations, 2);
//    
//    /*  Define a Starting and Endpoints for gradient (required parameter for gradient draw function */
//    CGPoint startPoint = { imageRect.origin.x + imageRect.size.width / 2, imageRect.origin.y };
//    CGPoint endPoint = { imageRect.origin.x + imageRect.size.width / 2, imageRect.origin.y + imageRect.size.height };
//    
//    /*  Define Triangle-Shaped Clipping Area for Gradient (by default they draw entire view frame) */
//    /*** USING CoreGraphics API in favor of Bezier object to build and draw the clipping path (good CG Practice) ***
//     // Add new subpath to existing Bezier Path to draw a triangle (a clipping path for the gradient
//     // Start out at tip point of triangle
//     [path moveToPoint:startPoint];
//     // Bottom Right point
//     [path addLineToPoint:CGPointMake(imageRect.origin.x + imageRect.size.width, imageRect.origin.y + imageRect.size.height)];
//     // Bottom Left point
//     [path addLineToPoint:CGPointMake(imageRect.origin.x, imageRect.origin.y + imageRect.size.height)];
//     // Close last segment between first and last CGPoints
//     [path closePath];
//     // Draw the lines for the clipping
//     path.lineWidth = 1; // thickness of line to 1 point
//     // Set line color and stroke
//     [[UIColor clearColor] setStroke];
//     // Draw the line
//     [path stroke];
//     // Making the path a "clipping" for the gradient via 'addClip' method
//     [path addClip];  ***/
//    
//    /*  Define Triangle Path */
//    // Coordinates / Path for triangle
//    CGPoint trianTop = CGPointMake(imageRect.origin.x + imageRect.size.width / 2, imageRect.origin.y);
//    CGPoint trianRight = CGPointMake(imageRect.origin.x + imageRect.size.width, imageRect.origin.y + imageRect.size.height);
//    CGPoint trianLeft = CGPointMake(imageRect.origin.x, imageRect.origin.y + imageRect.size.height);
//    
//    /*  Draw Triangle  */
//    // Build array of CGPoints for upcoming parameter
//    CGPoint lines[] = { trianTop, trianRight, trianLeft };
//    // Add line segments to current context per defined path
//    CGContextAddLines(currentContext, lines, 3); // this function draws segment with supplied parameters
//    /*  Make Clipping on Existing Context */
//    // Saving current context first
//    CGContextSaveGState(currentContext);
//    // Add Clipping path to current context
//    CGContextClip(currentContext);
//    
//    /*  Draw Gradient per Clipping */
//    CGContextDrawLinearGradient(currentContext, gradient, startPoint, endPoint,
//                                kCGGradientDrawsBeforeStartLocation & kCGGradientDrawsAfterEndLocation); // bitwised-AND last two contants together to form last argument
//    /* Release gradient and color space memory (previous C functions using 'create' need manual dealloc) */
//    CGGradientRelease(gradient);
//    CGColorSpaceRelease(colorSpace);
//    
//    /* Restore CurrentContext (everything drawn after this does not get the gradient) */
//    CGContextRestoreGState(currentContext);
//    //////// FINISH DRAW A GRADIENT //////////
//    
//    
//    //////// BRONZE - START DRAW A LOGO IMAGE W / SHADOW //////////
//    /*  Define Image */
//    // instance of UIImage
//    UIImage *logoImage = [UIImage imageNamed:@"logo.png"]; // define image
//    
//    /*  Saving current context first - CoreGraphics API functions required here */
//    CGContextSaveGState(currentContext);
//    
//    /*  Apply a Shadow to curren Context - CoreGraphics API functions required here, no ObjC abstracted yet */
//    // Everything drawn within this context gets a shadow after this, the logo in this case
//    CGContextSetShadow(currentContext, CGSizeMake(4, 8), 3); //func takes 3 arguments: context, CGSize, and offset
//    
//    /*  Compositing UIImage to the view */
//    // Draw image on fixed rectangle
//    [logoImage drawInRect:imageRect];
//    
//    /*  Restoring currentContext (everything drawn after this does not get shadow) */
//    CGContextRestoreGState(currentContext);
//    //////// FINISH DRAW A LOGO IMAGE W / SHADOW //////////
//    
//    
///*
//    // Get current context and set the shadow
//    CGContextRef currentContext = UIGraphicsGetCurrentContext();
//    CGContextSaveGState(currentContext);
//    CGContextSetShadow(currentContext, CGSizeMake(4, 10), 3);
//    
//    // Drawing triangle
//    UIBezierPath *trianglePath = [[UIBezierPath alloc] init];
//    CGPoint triangleOne = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect) + 100);
//    CGPoint triangleTwo = CGPointMake(CGRectGetMaxX(rect) - 50, CGRectGetMaxY(rect) - 100);
//    CGPoint triangleThree = CGPointMake(CGRectGetMinX(rect) + 50, CGRectGetMaxY(rect) - 100);
//    [trianglePath moveToPoint:triangleOne];
//    [trianglePath addLineToPoint:triangleTwo];
//    [trianglePath addLineToPoint:triangleThree];
//    //adding clipping mask
//    [trianglePath addClip];
//    
//    
//    // gradient
//    CGFloat locations[2] = { 0.0, 1.0 };
//    CGFloat components[8] = { 2.0, 0.0, 4.0, 5.0,
//        1.0, 1.0, 0.0, 1.0};
//    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
//    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorspace, components, locations, 2);
//    CGPoint startpoint = CGPointMake(0, 0);
//    CGPoint endpoint = CGPointMake(bounds.size.width, bounds.size.height);
//    CGContextDrawLinearGradient(currentContext, gradient, startpoint, endpoint, 0);
//    CGGradientRelease(gradient);
//    CGColorSpaceRelease(colorspace);
//    
//    // BRONZE CHALLENGE
//    UIImage *logoImage = [UIImage imageNamed:@"logo.png"];
//    CGRect logoRect = CGRectMake(110, 210, 200, 300);
//    [logoImage drawInRect:logoRect];
//    
//    // Restore the context
//    CGContextRestoreGState(currentContext);   */
}

@end
