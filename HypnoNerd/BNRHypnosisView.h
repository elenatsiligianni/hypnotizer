//
//  BNRHypnosisView.h
//  Hypnosister
//
//  Created by Elena Tsiligianni on 14/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRHypnosisView : UIView

// SILVER CHALLENGE
-(void)selectCircleColor:(id)sender;

@end
