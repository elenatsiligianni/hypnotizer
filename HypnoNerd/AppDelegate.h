//
//  AppDelegate.h
//  HypnoNerd
//
//  Created by Elena Tsiligianni on 19/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

